package ua.pt.ilisten;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

public class ItemsPlayList extends AppCompatActivity {

    private static String jsonSongs;
    private static ListView listView;
    private static ArrayAdapter listAdapter;
    private static HashMap<String,String> allMusic;
    private static ArrayList<String> playlistSongs;
    private static boolean random = false;
    private ArrayList<String> musicNamesRandom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_playlist);
        listView = (ListView) findViewById(R.id.listView);
        Intent i = getIntent();

        jsonSongs= i.getStringExtra("values");
        jsonSongs = jsonSongs.replace("[","");
        jsonSongs = jsonSongs.replace("]","");
        jsonSongs = jsonSongs.replace("\"","");
        String[] songs = jsonSongs.split(",");
        playlistSongs = new ArrayList<String>(Arrays.asList(songs));
        listAdapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.textMusics, playlistSongs);
        listView.setAdapter(listAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(view.getContext(), MainActivity.class);

                i.putExtra("listName", playlistSongs);
                i.putExtra("position",position);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }



}