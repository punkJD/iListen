package ua.pt.ilisten;

/**
 * Created by JD on 15/12/2016.
 */

public class Model {

    private String name;
    private int value;
    public Model(String name, int value){
        this.name = name;
        this.value = value;
    }
    public String getName(){
        return this.name;
    }
    public int getValue(){
        return this.value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}

