package ua.pt.ilisten;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by JD on 15/12/2016.
 */

public class CustomAdapter extends ArrayAdapter<Model> {
    Model[] modelItems=null;
    ArrayList<String> selectedItems = new ArrayList<>();
    Context context;
    public CustomAdapter(Context context,Model[] resource){
        super(context,R.layout.row_item,resource);
        // TODO Auto-generated constructor stub
        this.context=context;
        this.modelItems=resource;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        // TODO Auto-generated method stub
        LayoutInflater inflater=((Activity)context).getLayoutInflater();
        convertView=inflater.inflate(R.layout.row_item,parent,false);
        TextView name=(TextView)convertView.findViewById(R.id.music);
        CheckBox cb=(CheckBox)convertView.findViewById(R.id.checkBox1);
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    selectedItems.add(modelItems[position].getName());
                }
                else{
                    selectedItems.remove(modelItems[position].getName());
                }
            }
        });
        name.setText(modelItems[position].getName());
        if(modelItems[position].getValue()==1) {

            cb.setChecked(true);
        }
        else
            cb.setChecked(false);
        return convertView;
    }

    public ArrayList<String> getSelectedItems(){
        return selectedItems;
    }

}