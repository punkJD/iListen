package ua.pt.ilisten;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class ShowPlaylists extends AppCompatActivity {

    private static ListView listView;
    private static ArrayAdapter listAdapter;
    private static HashMap<String, Object> musics = new HashMap<>();
    private static HashMap<String,String> allMusic;

    private Firebase mRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_playlists);
        listView = (ListView) findViewById(R.id.listView);
        final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        final String id = "" + tm.getDeviceId();
        Intent f = getIntent();
        allMusic = (HashMap<String, String>) f.getSerializableExtra("musicMap");
        Firebase.setAndroidContext(this);
        mRef = new Firebase("https://ilisten2-f8d46.firebaseio.com/" + id);

        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                musics = (HashMap<String,Object>) dataSnapshot.getValue();
                final ArrayList<String> values = new ArrayList<String>(musics.keySet());
                listAdapter = new ArrayAdapter<String>(ShowPlaylists.this, R.layout.list_item, R.id.textMusics, values);

                listView.setAdapter(listAdapter);


                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Intent i = new Intent(view.getContext(), ItemsPlayList.class);

                        i.putExtra("values", (String) musics.get(values.get(position)));
                        startActivity(i);
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
}