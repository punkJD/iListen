package ua.pt.ilisten;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

public class ShowArtists extends AppCompatActivity {
    private HashMap<String,ArrayList<String>> map;

    private static ListView listView;
    private static ArrayAdapter listAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_artists);
        listView = (ListView) findViewById(R.id.listView3);
        Intent i = getIntent();

        map =(HashMap<String, ArrayList<String>>) i.getSerializableExtra("map");
        Log.i("ENTROU", "1");
        listAdapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.textMusics, new ArrayList(map.keySet()));
        Log.i("ENTROU", "2");
        listView.setAdapter(listAdapter);
        Log.i("ENTROU", "3");
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(view.getContext(), ItemsPlayList2.class);
                intent.putExtra("values", map.get((new ArrayList<String>(map.keySet())).get(position)));
                startActivity(intent);
            }
        });
        Log.i("ENTROU", "4");
    }
}
