package ua.pt.ilisten;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static int position;
    private static MediaPlayer mp;
    private String name;
    private String path;
    private boolean isPlaying = false;
    private int length = 0;
    private boolean stop = true;
    private int positionList = 0;
    //private HashMap<String,String> musics;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 2;
    private static ListView listView;
    private static ArrayAdapter<String> listAdapter;
    private static ArrayList<String> musicPathList;
    private static ArrayList<String> musicNamesList;
    private static ArrayList<String> musicNamesRandom;
    //private static MediaPlayer mp;
    private static HashMap<String,String>  musics = new HashMap<>();
    private static boolean random = false;
    private Map<String, Object> td;
    private GoogleApiClient client;
    private ArrayList<String> musicTemp;
    private ArrayList<String> musicTemp2;
    private boolean first = true;
    private HashMap<String,ArrayList<String>> artistMap;
    private HashMap<String,ArrayList<String>> albumMap;
    private HashMap<String,ArrayList<String>> genresMap;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        listView = (ListView) findViewById(R.id.listView);
        Intent intent = getIntent();
        if (mp!=null){
            mp.release();
            mp = null;
        }
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }
        else{
            Log.i("ENTROU2", "Hello2");
            musicPathList = getPaths();
            musicNamesList = splitToMusicName(musicPathList);
            for(int i = 0; i <musicNamesList.size(); i++)
                musics.put(musicNamesList.get(i),musicPathList.get(i));
            musicTemp2 = musicNamesList;
        }
        if(intent.getStringArrayListExtra("listName")!=null){

            musicNamesList = intent.getStringArrayListExtra("listName");
            positionList = intent.getIntExtra("position ",0);
            name=musicNamesList.get(positionList);
            first = false;
            pause();

        }

        showMusic();

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        musicTemp = musicNamesList;
        organizeByArtists();
        organizeByAlbuns();
        organizeByGenres();
    }

    private void organizeByGenres(){
        genresMap = new HashMap<String,ArrayList<String>>();
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        for(String a: musicTemp2) {
            String filePath = musics.get(a);
            mmr.setDataSource(filePath);
            String genreName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE);
            if (genreName == null) {
                genreName = "Unknown";
            }
            if (!genresMap.keySet().contains(genreName)) {
                genresMap.put(genreName, new ArrayList<String>());
            }
            genresMap.get(genreName).add(a);
            Log.i("artista", genresMap.get(genreName).toString());
        }
    }

    private void organizeByAlbuns(){
        albumMap = new HashMap<String,ArrayList<String>>();
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        for(String a: musicTemp2) {
            String filePath = musics.get(a);
            mmr.setDataSource(filePath);
            String albumName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
            if (albumName == null) {
                albumName = "Unknown";
            }
            if (!albumMap.keySet().contains(albumName)) {
                albumMap.put(albumName, new ArrayList<String>());
            }
            albumMap.get(albumName).add(a);
            Log.i("artista", albumMap.get(albumName).toString());
        }
    }

    private void organizeByArtists(){
        artistMap = new HashMap<String,ArrayList<String>>();
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        for(String a: musicTemp2){
            String filePath = musics.get(a);
            mmr.setDataSource(filePath);
            String artistName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
            if(artistName == null){
                artistName = "Unknown";
            }
            if(!artistMap.keySet().contains(artistName)) {
                artistMap.put(artistName, new ArrayList<String>());
            }
            artistMap.get(artistName).add(a);
            Log.i("artista",artistMap.get(artistName).toString());
        }
    }

    public void showMusic(){
        if (random==false)
            listAdapter = new ArrayAdapter<>(this, R.layout.list_item, R.id.textMusics, musicNamesList);
        else
            listAdapter = new ArrayAdapter<>(this, R.layout.list_item, R.id.textMusics, musicNamesRandom);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                first = false;
                isPlaying = true;
                positionList = position;
                if (mp!=null){
                    mp.release();
                    mp = null;
                }
                mp = MediaPlayer.create(MainActivity.this, Uri.fromFile(new File(musics.get(musicNamesList.get(position)))));
                mp.start();
                TextView text = (TextView) findViewById(R.id.textView2);
                text.setText(musicNamesList.get(position));
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        next();
                    }
                });
            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {

                if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
                }

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) { // contacts-related task you need to do.
                    musicPathList = getPaths();
                    musicNamesList = splitToMusicName(musicPathList);
                    listAdapter = new ArrayAdapter<>(this, R.layout.list_item, R.id.textMusics, musicNamesList);
                    listView.setAdapter(listAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            isPlaying = true;
                            positionList = position;
                            first = false;
                            if (mp!=null){
                                mp.release();
                                mp = null;
                            }
                            mp = MediaPlayer.create(MainActivity.this, Uri.fromFile(new File(musics.get(musicNamesList.get(position)))));
                            mp.start();
                            TextView text = (TextView) findViewById(R.id.textView2);
                            text.setText(musicNamesList.get(position));
                            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    next();
                                }
                            });

                        }
                    });
                    // ATTENTION: This was auto-generated to implement the App Indexing API.
                    // See https://g.co/AppIndexing/AndroidStudio for more information.
                    client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
                } else {

                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    System.exit(1);
                }
            }
        }
    }

    private ArrayList<String> getPaths() {
        Cursor cursor = this.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                null,
                null,
                null,
                MediaStore.Audio.Media.TITLE + " ASC");
        ArrayList<String> musicPathList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {

                long id = cursor.getLong(0);
                String musicPath = cursor.getString(1);
                if (musicPath != null)
                    musicPathList.add(musicPath);
            }
            while (cursor.moveToNext());
        }
        return musicPathList;
    }

    private ArrayList<String> splitToMusicName(ArrayList<String> musicList) {
        ArrayList<String> musicNamesList = new ArrayList<>();
        for (String c : musicList) {
            if (c!=null) {
                String[] splited = c.split("/");
                musicNamesList.add(splited[splited.length - 1]);
            }
        }
        return musicNamesList;
    }



    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }



    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void pauseMusic(View view) {
        pause();
    }

    public void stopMusic(View view) {
        stop();
    }

    public void pause() {

//        if (stop) {
//            Button pause1 = (Button)findViewById(R.id.pauseButton);
//            pause1.setText("Play");
//            mp = MediaPlayer.create(this, Uri.fromFile(new File(musics.get(name))));
//            mp.start();
//            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                @Override
//                public void onCompletion(MediaPlayer mp) {
//                    next();
//                }
//            });
//            stop = false;
//        }
        if (stop && !first) {
            Button pause1 = (Button)findViewById(R.id.pauseButton);
            pause1.setText("Play");
            mp = MediaPlayer.create(this, Uri.fromFile(new File(musics.get(name))));
            mp.start();
            TextView text = (TextView) findViewById(R.id.textView2);
            text.setText(name);
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    next();
                }
            });
            stop = false;
        }
        if (isPlaying && !first) {
            Button pause1 = (Button)findViewById(R.id.pauseButton);
            pause1.setText("Play");
            mp.pause();
            length = mp.getCurrentPosition();

        } else if (!isPlaying && !first) {
            Button pause1 = (Button)findViewById(R.id.pauseButton);
            pause1.setText("Pause");
            mp.seekTo(length);
            mp.start();
        }
        if(first){
            first = false;
            positionList = 0;
            mp = MediaPlayer.create(MainActivity.this, Uri.fromFile(new File(musics.get(musicNamesList.get(positionList)))));
            mp.start();
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    next();
                }
            });
        }

        isPlaying = !isPlaying;

    }

    public void previous(){
        positionList -= 1;
        if (positionList < 0) {
            positionList = musicNamesList.size() - 1;
        }
        name = musicNamesList.get(positionList);
        boolean wasPlaying = isPlaying;
        stop();
        if(wasPlaying){
            pause();
        }
    }

    public void next(){
        positionList = positionList + 1;
        if (positionList >= musicNamesList.size() ){
            positionList = 0;
        }

        name = musicNamesList.get(positionList);
        boolean wasPlaying = isPlaying;
        stop();
        if(wasPlaying){

            pause();
        }
    }

    public void stop() {
        Button pause1 = (Button)findViewById(R.id.pauseButton);
        pause1.setText("Play");
        length = 0;
        if(mp!=null) {
            mp.stop();
            mp.release();
            mp = null;
        }
        isPlaying = false;
        stop = true;


    }


    public void nextMusic(View view) {
        next();
    }

    public void previousMusic(View view) {
        previous();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.create_playlist) {
            Intent intent = new Intent(this, PlayListActivity.class);
            startActivity(intent);
        } else if (id == R.id.show_playlist) {
            Intent intent = new Intent(this, ShowPlaylists.class);
            startActivity(intent);
        } else if (id == R.id.artists) {
            Intent intent = new Intent(this, ShowArtists.class);
            intent.putExtra("map",(Serializable) artistMap);
            startActivity(intent);
        } else if (id == R.id.albums) {
            Intent intent = new Intent(this, ShowArtists.class);
            intent.putExtra("map",(Serializable) albumMap);
            startActivity(intent);

        } else if (id == R.id.genres) {
            Intent intent = new Intent(this, ShowArtists.class);
            intent.putExtra("map",(Serializable) genresMap);
            startActivity(intent);
        } else if (id == R.id.all_musics){
            musicNamesList = musicTemp2;
            showMusic();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void shuffle(View view){
        String nameTemp = musicNamesList.get(positionList);
        if (random==false) {
            random = true;
            long seed = System.nanoTime();
            musicNamesRandom = new ArrayList<>();
            musicNamesRandom.addAll(musicNamesList);
            Collections.shuffle(musicNamesRandom, new Random(seed));
            for(int i = 0; i < musicNamesRandom.size(); i++ ){
                if(musicNamesRandom.get(i) == nameTemp){
                    positionList = i;
                    break;
                }
            }
            musicTemp = musicNamesList;
            musicNamesList = musicNamesRandom;
            showMusic();
        }
        else {
            random = false;
            musicNamesList = musicTemp;
            for(int i = 0; i < musicNamesList.size(); i++ ){
                if(musicNamesList.get(i) == nameTemp){
                    positionList = i;
                    break;
                }
            }

            showMusic();
        }

    }

}
