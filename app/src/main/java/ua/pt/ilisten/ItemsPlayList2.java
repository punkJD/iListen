package ua.pt.ilisten;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ItemsPlayList2 extends AppCompatActivity {
    ArrayList<String> musics;
    private static ListView listView;
    private static ArrayAdapter listAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("puta","crl");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_play_list2);
        listView = (ListView) findViewById(R.id.listView2);
        Intent i = getIntent();
        musics = i.getStringArrayListExtra("values");
        listAdapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.textMusics, musics);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(view.getContext(), MainActivity.class);

                i.putExtra("listName", musics);
                i.putExtra("position",position);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

    }
}
