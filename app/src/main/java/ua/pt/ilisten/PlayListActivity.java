package ua.pt.ilisten;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.firebase.client.Firebase;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

public class PlayListActivity extends AppCompatActivity {
    private static ArrayList<String> musicPathList;
    private static ArrayList<String> musicNamesList;
    private static ArrayList<String> playlist =new ArrayList<String>();
    private static HashMap<String, String> mapMusics;
    private static ListView lv;
    private static Model[] modelItems;
    private static CustomAdapter adapter;
    private Firebase mRef;
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);
        Firebase.setAndroidContext(this);
        mRef = new Firebase("https://ilisten-287b5.firebaseio.com/");
        Intent intent = getIntent();
        musicPathList = getPaths();
        musicNamesList = splitToMusicName(musicPathList);

        mapMusics = getHashMap(musicPathList, musicNamesList);

        lv = (ListView) findViewById(R.id.listView1);
        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        modelItems = new Model[musicNamesList.size()];

        for(int i=0; i<modelItems.length; i++){
            modelItems[i] = new Model(musicNamesList.get(i), 0);
        }

        adapter = new CustomAdapter(this, modelItems);
        lv.setAdapter(adapter);

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();


    }



    private HashMap<String, String> getHashMap(ArrayList<String> paths, ArrayList<String> names){
        HashMap<String, String> map = new HashMap<>();
        for(int i=0; i<paths.size(); i++){
            map.put(names.get(i), paths.get(i));
        }

        return map;
    }

    private ArrayList<String> getPaths() {
        Cursor cursor = this.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                null,
                null,
                null,
                MediaStore.Audio.Media.TITLE + " ASC");
        ArrayList<String> musicPathList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {

                long id = cursor.getLong(0);
                String musicPath = cursor.getString(1);
                if (musicPathList != null)
                    musicPathList.add(musicPath);
            }
            while (cursor.moveToNext());
        }
        return musicPathList;
    }

    private ArrayList<String> splitToMusicName(ArrayList<String> musicList) {
        ArrayList<String> musicNamesList = new ArrayList<>();
        for (String c : musicList) {
            if (c!=null) {
                String[] splited = c.split("/");
                musicNamesList.add(splited[splited.length - 1]);
            }
        }
        return musicNamesList;
    }

    public void createPlayList(View view) {
        playlist = adapter.getSelectedItems();
        final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        final String id = "" + tm.getDeviceId();
        EditText et = (EditText)findViewById(R.id.editText);
        String nome = et.getText().toString();
        Firebase child = mRef.child(id);
        Firebase nmRef = new Firebase("https://ilisten2-f8d46.firebaseio.com/"+ id);
        JSONArray musics = new JSONArray(playlist);

        Firebase child1 = nmRef.child(nome);

        child1.setValue(musics.toString());
        super.onBackPressed();
    }





        /*Intent intent = new Intent(this, ConfirmationPlayList.class);
        intent.putStringArrayListExtra("checkBox", playlist);
        intent.putStringArrayListExtra("names", musicNamesList);
        intent.putExtra("mapa", mapMusics);
        startActivity(intent);*/






        /*ListAdapter la = lv.getAdapter();

        for(int i=0; i<la.getCount(); i++){
            Model model = (Model) la.getItem(i);
            if (model.getValue() == 1){
                playlist.add(model.getName());
            }
            Log.i("List:", ""+model.getValue());

        }*/

    public void clearText(View view){

        EditText editText = (EditText)findViewById(R.id.editText);
        editText.setText("");

    }

}
